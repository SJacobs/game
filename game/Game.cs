﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace game
{
    public partial class Game : Form
    {
        private World world;

        public Game()
        {
            InitializeComponent();
            world = new World();
            DoubleBuffered = true;
            updateTimer.Enabled = true;
        }

        private void Game_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            world.draw(graphics);
        }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            world.update();
            Refresh();
        }
    }
}
