﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class Cell
    {
        public Point index { get; set; }
        public Point position { get; set; }
        public CellType type { get; set; }  

        public Cell(Point index, Point position, CellType type)
        {
            this.index = index;
            this.position = position;
            this.type = type;
        }
        
        private void draw(Graphics g)
        {

        }  
    }
}
