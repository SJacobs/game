﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class Player
    {
        public Point position { get; set; }
        public bool powerUp { get; set; }
        public int hitPoints { get; set; }

        public Player(Point position)
        {
            this.position = position;
            powerUp = false;
            hitPoints = 100;
        }

        public void update()
        {

        }

        public void draw(Graphics graphics)
        {
            Pen pen = Pens.Black;
            Brush brush = Brushes.Black;
            Rectangle rectangle = new Rectangle(position, new Size(50, 50));

        //    graphics.DrawEllipse(pen, rectangle);
            graphics.FillEllipse(brush, rectangle);
        }

        private void interaction(int keyCode)
        {

        }
    }
}
