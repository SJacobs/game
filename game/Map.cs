﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class Map
    {
        public Size mapSize { get; set; }
        public Size cellSize { get; set; }
        public Size cellCount { get; set; }
        public Point goalPosition { get; set; }

        public List<Cell> cells { get; private set; }

        public Map(Point goalPosition)
        {
            mapSize = new Size(1000, 1000);
            cellSize = new Size(50, 50);
            cellCount = new Size(10, 10);
            this.goalPosition = goalPosition;
            cells = new List<Cell>();
            cells.Add(new Cell());
        }

        public void draw(Graphics graphics)
        {
            Pen pen = Pens.Black;

            const int START_X = 50;
            const int START_Y = 50;

            int x = START_X;
            int y = START_Y;

            for (int i = 0; i < cellCount.Width + 1; i++)
            {
                graphics.DrawLine(pen, x, START_Y, x, START_Y + (cellSize.Height * cellCount.Height));
                x += cellSize.Width;
            }

            for (int i = 0; i < cellCount.Height + 1; i++)
            {
                graphics.DrawLine(pen, START_X, y, START_X + (cellSize.Width * cellCount.Width), y);
                y += cellSize.Height;
            }
        }
    }
}
