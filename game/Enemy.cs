﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class Enemy
    {
        public Point position { get; set; }
        public int hitPoints { get; set; }

        public Enemy(Point position)
        {
            this.position = position;
            hitPoints = 100;
        }

        public void update()
        {

        }

        public void draw(Graphics graphics)
        {
            Pen pen = Pens.Red;
            Brush brush = Brushes.Red;
            Rectangle rectangle = new Rectangle(position, new Size(50, 50));

           // graphics.DrawEllipse(pen, rectangle);
            graphics.FillEllipse(brush, rectangle);
        }

        private void attack(Player player)
        {

        }
    }
}
