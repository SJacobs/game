﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    enum Action
    {
        NoAction,
        MoveUp,
        MoveRight,
        MoveDown,
        MoveLeft,
        PerformAction
    }
}
