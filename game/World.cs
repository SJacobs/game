﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class World
    {
        public bool gameWon { get; private set; }
        public bool gameOver { get; private set; }

        public Player player { get; private set; }

        public List<Enemy> enemies { get; private set; }

        public Map map { get; private set; }

        public World()
        {
            gameWon = false;
            gameOver = false;
            player = new Player(new Point(10, 10));
            enemies = new List<Enemy>();
            enemies.Add(new Enemy(new Point(100, 10)));
            map = new Map(new Point(200, 10));
        }

        public void update()
        {
            player.update();
            foreach (Enemy enemy in enemies)
                enemy.update();
        }

        public void draw(Graphics graphics)
        {
            map.draw(graphics);
            player.draw(graphics);
            foreach (Enemy enemy in enemies)
                enemy.draw(graphics);
        }
    }
}
